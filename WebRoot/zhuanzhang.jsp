<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'yemian.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body >
    	<center>
			<div>卡号：${a.cardno } <a href="account/tuichu">退出登录</a></div>    	
    		
    		<div style="clear: both;">
    			<div style="float:left; margin-left: 80px;">
	    			<a href="account/chayue?cardno=${a.cardno }">查询</a><br/>
	    			<a href="zhuanzhang.jsp">转账</a><br/>
	    			<a href="chaxunyemian.jsp">查询交易记录</a><br/>
	    			<a href="xiugaimima.jsp">修改密码</a><br/>
    			</div>
    		
    		<div style="width: 650;height: 300; float:inherit; border: 5px solid green; ">
    			<br/><br/><br/><br/><br/><h2>当前操作：转账。请输入转入账号和金额后点“转账”按钮</h2>
    			<form action="account/zhuanzhang" method="post">
    			<input type="hidden" name="cardno1" value="${a.cardno }"/>
    			<input type="hidden" name="balance1" value="${a.balance }"/>
    					<table>
    						<tr>
    							<td>转入账号</td>
    							<td><input type="text" name="cardno"/></td>
    						</tr>
    						<tr>
    							<td>转账金额</td>
    							<td><input type="text" name="balance"/></td>
    						</tr>
    						<tr>
    							<td colspan="2"><input type="submit" id="zhuanzhang" value="转账"/></td>
    						</tr>
    					</table>
    			</form>
    		</div>
    		
    		</div>
    		
    	</center>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
    	$("[name=cardno]").blur(function(){
    		if($("[name=cardno]").val().length!=16){
    		alert("请正确填写您的16位卡号");
    		$("[name=cardno]").val("");
    			return false;
    		}
    	});
    	$("[name=balance]").blur(function(){
    		if(isNaN($("[name=balance]").val())==true){
    		alert("请正确填写转账金额");
    		$("[name=balance]").val("");
    			return false;
    		}
    	});
    	
    	$("#zhuanzhang").click(function(){
    		$.post("account/zhanghaoshifoucunzai","cardno="+$("[name=cardno]").val(),function(data){
    			if(data=="0"){
    				alert("登录失败，您输入的卡号不存在");
    				location.href="/AccountSystem/zhuanzhang.jsp";
    				return false;
    			}
    		},"text");
    	});
    		
    	
    </script>
  </body>
</html>
