<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'yemian.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body >
  	<c:if test="${tlist==null }">
  		<jsp:forward page="transactionrecord/chafenye"/>
  	</c:if>
    	<center>
			<div>卡号：${a.cardno } <a href="account/tuichu">退出登录</a></div>    	
    		
    		<div style="clear: both;">
    			<div style="float:left; margin-left: 80px;">
	    			<a href="account/chayue?cardno=${a.cardno }">查询</a><br/>
	    			<a href="zhuanzhang.jsp">转账</a><br/>
	    			<a href="chaxunyemian.jsp">查询交易记录</a><br/>
	    			<a href="xiugaimima.jsp">修改密码</a><br/>
    			</div>
    		
    		<div style="width: 660;height: 500; float:inherit; border: 5px solid green;">
    			<br/><br/><br/><br/><br/><h2>当前操作：查询交易记录请选择时间范围后点查询按钮</h2>
    			<form action="transactionrecord/chafenye" method="post">
    				查询日期范围<input type="text" name="transaction" value="${transaction }" onclick="new WdatePicker()"
     			 class="Wdate" readonly="readonly"/> 
     			 <input type="text" name="transaction1" value="${transaction1 }" onclick="new WdatePicker()"
     			 class="Wdate" readonly="readonly"/> <input type="submit" value="查询"/>
    			</form>
    			<table border="1" cellspacing="0" width="500" >
    				<tr>
    					<th>交易日期</th>
    					<th>支出</th>
    					<th>存入</th>
    					<th>账户余额</th>
    					<th>交易类型</th>
    					<th>备注</th>
    				</tr>
    				<c:forEach var="t" items="${tlist }">
    					<tr>
	    					<td>${t.transaction }</td>
	    					<td>${t.expense }</td>
	    					<td>${t.income }</td>
	    					<td>${t.balance1 }</td>
	    					<td>${t.transactionType }</td>
	    					<td>${t.remark }</td>
	    				</tr>
    				</c:forEach>
    				
    				
    			</table>
    						
  		<c:choose>
  			<c:when test="${curPage==1 }">
  				首页 | 下一页
  			</c:when>
  			<c:otherwise>
  				<a href="transactionrecord/chafenye?number=1">首页</a> |<a href="transactionrecord/chafenye?number=${curPage-1 }">下一页</a> 
  			</c:otherwise>
  		</c:choose>
  		<c:choose>
  			<c:when test="${curPage==totolPage }">
  				上一页 | 末页
  			</c:when>
  			<c:otherwise>
  				<a href="transactionrecord/chafenye?number=${curPage+1 }">上一页</a> |<a href="transactionrecord/chafenye?number=${totolPage }">末页</a> 
  			</c:otherwise>
  		</c:choose>
  		第${curPage }页/总共${totolPage }页 （总条数：${row }）
    			
    		</div>
    		
    		</div>
    		
    	</center>
    <script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
  </body>
</html>
