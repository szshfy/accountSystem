package com.bdqn.accountsystem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bdqn.accountsystem.bean.TransactionRecord;

public interface TransactionRecordMapper {

	/**
	 * 添加
	 * @param t
	 * @return
	 */
	int insert(TransactionRecord t);

	/**
	 * 查询总条数
	 * @param transaction
	 * @param transaction1
	 * @return
	 */
	int count(@Param("transaction") String transaction,
			@Param("transaction1")String transaction1);

	List<TransactionRecord> getAll(@Param("start")int start,@Param("pageSize")int pageSize,@Param("transaction") String transaction,
			@Param("transaction1")String transaction1);

}
