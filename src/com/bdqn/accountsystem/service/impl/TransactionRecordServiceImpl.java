package com.bdqn.accountsystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bdqn.accountsystem.bean.TransactionRecord;
import com.bdqn.accountsystem.mapper.TransactionRecordMapper;
import com.bdqn.accountsystem.service.TransactionRecordService;
@Service
public class TransactionRecordServiceImpl implements TransactionRecordService{
	@Autowired
	private TransactionRecordMapper transactionRecordMapper;

	@Override
	public int count(String transaction, String transaction1) {
		
		return transactionRecordMapper.count(transaction,transaction1);
	}

	@Override
	public List<TransactionRecord> getAll(int curPage, int pageSize,
			String transaction, String transaction1) {
		return transactionRecordMapper.getAll((curPage-1)*pageSize,pageSize,transaction,transaction1);
	
	}
	
}
