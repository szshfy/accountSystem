package com.bdqn.accountsystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bdqn.accountsystem.bean.Account;
import com.bdqn.accountsystem.bean.TransactionRecord;
import com.bdqn.accountsystem.mapper.AccountMapper;
import com.bdqn.accountsystem.mapper.TransactionRecordMapper;
import com.bdqn.accountsystem.service.AccountService;
@Service
@Transactional
public class AccountServiceImpl implements AccountService{
	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private TransactionRecordMapper transactionRecordMapper;

	@Override
	public int zhanghaoshifoucunzai(String cardno) {
		return accountMapper.zhanghaoshifoucunzai(cardno);
	}

	@Override
	public int chamimashifoucuowu(String password,String cardno) {
		return accountMapper.chamimashifoucuowu(password,cardno);
	}

	@Override
	public Account alist(String cardno, String password) {
		Account a=	accountMapper.alist(cardno);
		if(a!=null && a.getPassword().equals(password)){
			return a;
		}
		return null;
	}

	@Override
	public Account chayue(String cardno) {
		return accountMapper.chayue(cardno);
	}

	@Override
	public int zhuanzhang(String cardno, double balance, String cardno1,
			double balance1) {
		
		Account a=accountMapper.chaByid(cardno1);
		int row=0;
		if(a.getBalance()>=balance){
			a.setBalance(a.getBalance()-balance);
			row=accountMapper.update(a);
		}
		Account a1=accountMapper.chaByid(cardno);
		a1.setBalance(a1.getBalance()+balance);
		int row1=accountMapper.update(a1);
		TransactionRecord t=new TransactionRecord();
		t.setBalance(balance1);
		t.setCardno(cardno1);
		
		int row2=transactionRecordMapper.insert(t);
		if(row1!=0 && row!=0 && row2!=0){
			return 1;
		}
		return 0;
		
		
	}

	@Override
	public int updatePwd(String cardno, String newpassword2) {
		return accountMapper.updatePwd(cardno,newpassword2);
	}
	
	
	
	
}
