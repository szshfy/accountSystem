package com.bdqn.accountsystem.service;

import java.util.List;

import com.bdqn.accountsystem.bean.TransactionRecord;

public interface TransactionRecordService {

	/**
	 * 查询条数
	 * @param transaction
	 * @param transaction1
	 * @return
	 */
	int count(String transaction, String transaction1);

	/**
	 * 查询集合
	 * @param curPage
	 * @param pageSize
	 * @param transaction
	 * @param transaction1
	 * @return
	 */
	List<TransactionRecord> getAll(int curPage, int pageSize,
			String transaction, String transaction1);

}
