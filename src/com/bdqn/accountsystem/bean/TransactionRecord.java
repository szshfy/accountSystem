package com.bdqn.accountsystem.bean;

public class TransactionRecord {

	private int id;//  INT PRIMARY KEY AUTO_INCREMENT,#记录id
	private String cardno;// VARCHAR(16) NOT NULL,#对应account
	//FOREIGN KEY (cardno) REFERENCES account(cardno),
	private String transaction;//date DATE NOT NULL,#取系统时间
	private double expense;// FLOAT(11,2), #支出的金额
	private double income;// FLOAT(11,2),#存入的金额
	private double balance;// FLOAT(11,2) NOT NULL,#交易完成时的余额
	private String transactionType;// VARCHAR(10) NOT NULL,#存款 取款 消费
	private String  remark;// VARCHAR(20)#备注
	private double balance1;
	
	public double getBalance1() {
		return balance1;
	}
	public void setBalance1(double balance1) {
		this.balance1 = balance1;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCardno() {
		return cardno;
	}
	public void setCardno(String cardno) {
		this.cardno = cardno;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public double getExpense() {
		return expense;
	}
	public void setExpense(double expense) {
		this.expense = expense;
	}
	public double getIncome() {
		return income;
	}
	public void setIncome(double income) {
		this.income = income;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
