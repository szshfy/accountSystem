package com.bdqn.accountsystem.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdqn.accountsystem.bean.Account;
import com.bdqn.accountsystem.service.AccountService;

@Controller
@RequestMapping("account")
public class AccountController {
	@Autowired
	private AccountService accountService;
	@RequestMapping(value="zhanghaoshifoucunzai",produces="application/text;charset=UTF-8;")
	@ResponseBody
	public String zhanghaoshifoucunzai(String cardno){
		int row =accountService.zhanghaoshifoucunzai(cardno);
		System.out.println(row);
		return ""+row;
	}
	@RequestMapping(value="chamimashifoucuowu",produces="application/text;charset=UTF-8;")
	@ResponseBody
	public String chamimashifoucuowu(String password,String cardno){
		int row=accountService.chamimashifoucuowu(password,cardno);
		return ""+row;
	}
	@RequestMapping("alist")
	public String alist(String cardno,String password,HttpSession session){
		Account a=accountService.alist(cardno,password);
		session.setAttribute("a",a);
		return "yemian";
	}
	@RequestMapping("tuichu")
	public void tuichu(HttpSession session,HttpServletResponse response) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		session.invalidate();
		response.getWriter().print("<script>alert('退出成功');location.href='/AccountSystem/index.jsp';</script>");
		
	}
	@RequestMapping("chayue")
	public String chayue(String cardno,Model m){
		Account a=accountService.chayue(cardno);
		m.addAttribute("a",a);
		return "chayue";
	}
	
	@RequestMapping("zhuanzhang")
	public void zhuanzhang(String cardno,double balance,String cardno1,double balance1,HttpServletResponse response) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		int row=accountService.zhuanzhang(cardno,balance,cardno1,balance1);
		if(row==1){
			response.getWriter().print("<script>alert('转账成功');location.href='/AccountSystem/tishi.jsp';</script>");
		}
	}
	
	@RequestMapping("updatePwd")
	public void updatePwd(String cardno,String newpassword2,HttpServletResponse response) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		int row =accountService.updatePwd(cardno,newpassword2);
		if(row>0){
			response.getWriter().print("<script>alert('修改密码成功');location.href='/AccountSystem/tishi.jsp';</script>");
		}
	}
}
