package com.bdqn.accountsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bdqn.accountsystem.bean.TransactionRecord;
import com.bdqn.accountsystem.service.TransactionRecordService;

@Controller
@RequestMapping("transactionrecord")
public class TransactionRecordController {
	@Autowired
	private TransactionRecordService transactionRecordService;
	@RequestMapping("chafenye")
	public String chafenye(@RequestParam(value="number",required=false,defaultValue="1")int curPage,
			@RequestParam(value="transaction",required=false)String transaction
			,@RequestParam(value="transaction1",required=false)String transaction1
			,Model m){
		int PageSize=5;
		int row=transactionRecordService.count(transaction,transaction1);
		int totolPage=row/PageSize+(row%PageSize==0?0:1);
		List<TransactionRecord> list=transactionRecordService.getAll(curPage,PageSize,transaction,transaction1);
		m.addAttribute("transaction",transaction);
		m.addAttribute("transaction1",transaction1);
		m.addAttribute("tlist",list);
		m.addAttribute("curPage",curPage);
		m.addAttribute("totolPage", totolPage);
		m.addAttribute("row",row);
		return "chaxunyemian";
	}
	
}
